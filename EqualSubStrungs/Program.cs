﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EqualSubStrungs
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = new[] {1, 2, 3, 4, 6};
            bool[,] binary = CreateBinarySet(input);
            List<List<int>> equalLists = CheckEqualSubSets(binary, input);

            for (int i = 0; i < equalLists.Count; i++)
            {
                Console.WriteLine("List #" + i + ":");

                for (int j = 0; j < equalLists[i].Count; j++)
                {
                    Console.Write(equalLists[i][j]+", ");
                }
                Console.WriteLine();
            }

            Console.ReadLine();
        }

        private static bool[,] CreateBinarySet(int[] input)
        {
            bool[,] ret = new bool[(int)Math.Pow(2, input.Length), input.Length];

            for (int i = 0; i < ret.GetLength(0); i++)
            {
                int temp = i;
                for (int j = ret.GetLength(1) - 1; j >= 0; j--)
                {
                    ret[i, j] = temp % 2 == 0;
                    temp /= 2;
                }
            }

            return ret;
        }

        private static List<List<int>> CheckEqualSubSets(bool[,] selectors, int[] input)
        {
            List<List<int>> ret = new List<List<int>>();
            for (int i = 0; i < selectors.GetLength(0); i++)
            {
                List<int> left = new List<int>();
                List<int> right = new List<int>();

                for (int j = 0; j < selectors.GetLength(1); j++)
                {
                    if (selectors[i, j])
                    {
                        left.Add(input[j]);
                    }
                    else
                    {
                        right.Add(input[j]);
                    }
                }

                if (left.Sum(x => x) == right.Sum(y => y))
                {
                    ret.Add(left);
                    ret.Add(right);
                    return ret;
                }
            }
                
            return ret;
        }
    }
}
